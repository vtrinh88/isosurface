//
//  MarchingCubes.h
//  Project 3
//
//  Created by Viet Trinh on 3/12/13.
//  Copyright (c) 2013 Viet Trinh. All rights reserved.
//

#ifndef Project_3_MarchingCubes_h
#define Project_3_MarchingCubes_h

#include "MCTable.h"
#include "PointAndVector.h"

class Triangle{
public:
    Triangle(){}
    Point vertices[3];
};

Vector IntersectionPointCalculation(MCPoint p1, MCPoint p2, float val);
Triangle* MarchingCubes(int ncellsX, int ncellsY, int ncellsZ, float isoval, MCPoint * MCData, int &numMCTriangles);

#endif
