//
//  PointAndVector.h
//  Project 3
//
//  Created by Viet Trinh on 3/12/13.
//  Copyright (c) 2013 Viet Trinh. All rights reserved.
//

#ifndef Project_3_PointAndVector_h
#define Project_3_PointAndVector_h

/*----- CLASSES DECLARATION ----------*/
//=== class Point ===
class Point{
    float x;
    float y;
    float z;
public:
    Point(){x=0;y=0;z=0;}
    Point(float xp,float yp,float zp){x = xp;y=yp;z=zp;}
    float getXCoord(){return x;}
    float getYCoord(){return y;}
    float getZCoord(){return z;}
    void setX(float xp){x=xp;}
    void setY(float yp){y=yp;}
    void setZ(float zp){z=zp;}
    void setXYZCoord(float xp,float yp,float zp){x = xp;y=yp;z=zp;}
};


class MCPoint: public Point{
    float value;
public:
    MCPoint():Point(){value=0;}
    MCPoint(float xp,float yp,float zp,float vp):Point(xp,yp,zp){value=vp;}
    void setValue(float vp){value=vp;}
    float getValue(){return value;}
};

//=== class Vector ===
class Vector{
public:
    float x;
    float y;
    float z;
    
    Vector(){x = 0.0;y=0.0;z=0.0;}
};

#endif
