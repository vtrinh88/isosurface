//
//  MarchingCubes.cpp
//  Project 3
//
//  Created by Viet Trinh on 3/12/13.
//  Copyright (c) 2013 Viet Trinh. All rights reserved.
//

#include "MarchingCubes.h"
#include <cstdlib>
#include <cstdio>

int pointsZ;	//number of points on Z zxis (equal to ncellsZ+1)
int YtimeZ;		//'plane' of cubes on YZ (equal to (ncellsY+1)*pointsZ )

//=========================================================
Vector IntersectionPointCalculation(MCPoint p1, MCPoint p2, float val){
    Vector ret;
    float coef = (val - p1.getValue())/(p2.getValue() - p1.getValue());
    
    ret.x = (p2.getXCoord()-p1.getXCoord())*coef + p1.getXCoord();
    ret.y = (p2.getYCoord()-p1.getYCoord())*coef + p1.getYCoord();
    ret.z = (p2.getZCoord()-p1.getZCoord())*coef + p1.getZCoord();
    
    return ret;
}

//=========================================================
Triangle* MarchingCubes(int ncellsX, int ncellsY, int ncellsZ, float isoval, MCPoint * MCData, int &numMCTriangles){
    
    Triangle* listTriangle = new Triangle[3*ncellsX*ncellsY*ncellsZ];
    numMCTriangles = (int)0;
    
    int ncellsYZ = (ncellsY+1)*(ncellsZ+1);
    
    // loop through all points
    for (int i=0; i<ncellsX; i++) {
        for (int j=0; j<ncellsY; j++) {
            for (int k=0; k<ncellsZ; k++) {
                
                //initialize vertices
                MCPoint verts[8];
                int index = i*ncellsYZ + j*(ncellsZ) + k;
                
                verts[0] = MCData[index];
                verts[1] = MCData[index + ncellsYZ];
				verts[2] = MCData[index + ncellsYZ + 1];
				verts[3] = MCData[index + 1];
				verts[4] = MCData[index + (ncellsZ+1)];
				verts[5] = MCData[index + ncellsYZ + (ncellsZ+1)];
				verts[6] = MCData[index + ncellsYZ + (ncellsZ+1) + 1];
				verts[7] = MCData[index + (ncellsZ+1) + 1];
                
                //get the index
				int cubeIndex = int(0);
                for (int n=0; n<8; n++) {
                    float l_val = verts[n].getValue();
                    if (l_val <= isoval) {
                        cubeIndex |= (1 << n);
                    }
                }
                
                //check if its completely inside or outside
                if(!edgeTable[cubeIndex]) continue;
                
                //get intersection vertices on edges and save into the array
                Vector intersection_Verts[12];
                if(edgeTable[cubeIndex] & 1) intersection_Verts[0] = IntersectionPointCalculation(verts[0], verts[1], isoval);
				if(edgeTable[cubeIndex] & 2) intersection_Verts[1] = IntersectionPointCalculation(verts[1], verts[2], isoval);
				if(edgeTable[cubeIndex] & 4) intersection_Verts[2] = IntersectionPointCalculation(verts[2], verts[3], isoval);
				if(edgeTable[cubeIndex] & 8) intersection_Verts[3] = IntersectionPointCalculation(verts[3], verts[0], isoval);
				if(edgeTable[cubeIndex] & 16) intersection_Verts[4] = IntersectionPointCalculation(verts[4], verts[5], isoval);
				if(edgeTable[cubeIndex] & 32) intersection_Verts[5] = IntersectionPointCalculation(verts[5], verts[6], isoval);
				if(edgeTable[cubeIndex] & 64) intersection_Verts[6] = IntersectionPointCalculation(verts[6], verts[7], isoval);
				if(edgeTable[cubeIndex] & 128) intersection_Verts[7] = IntersectionPointCalculation(verts[7], verts[4], isoval);
				if(edgeTable[cubeIndex] & 256) intersection_Verts[8] = IntersectionPointCalculation(verts[0], verts[4], isoval);
				if(edgeTable[cubeIndex] & 512) intersection_Verts[9] = IntersectionPointCalculation(verts[1], verts[5], isoval);
				if(edgeTable[cubeIndex] & 1024) intersection_Verts[10] = IntersectionPointCalculation(verts[2], verts[6], isoval);
				if(edgeTable[cubeIndex] & 2048) intersection_Verts[11] = IntersectionPointCalculation(verts[3], verts[7], isoval);
                
                //build the triangles using triTable
                for (int n=0; triTable[cubeIndex][n] != -1; n+=3) {
                    listTriangle[numMCTriangles].vertices[0].setXYZCoord(intersection_Verts[triTable[cubeIndex][n+2]].x,
                                                                         intersection_Verts[triTable[cubeIndex][n+2]].y,
                                                                         intersection_Verts[triTable[cubeIndex][n+2]].z);
					listTriangle[numMCTriangles].vertices[1].setXYZCoord(intersection_Verts[triTable[cubeIndex][n+1]].x,
                                                                         intersection_Verts[triTable[cubeIndex][n+1]].y,
                                                                         intersection_Verts[triTable[cubeIndex][n+1]].z);
					listTriangle[numMCTriangles].vertices[2].setXYZCoord(intersection_Verts[triTable[cubeIndex][n]].x,
                                                                         intersection_Verts[triTable[cubeIndex][n]].y,
                                                                         intersection_Verts[triTable[cubeIndex][n]].z);
                    
					numMCTriangles++;
				}
            }
        }
    }
    
    
    //free all the wasted space
    Triangle* retListTriangle = new Triangle[numMCTriangles];
    for (int i=0; i<numMCTriangles; i++) {
        retListTriangle[i] = listTriangle[i];
    }
    delete [] listTriangle;
    
    return retListTriangle;
}
