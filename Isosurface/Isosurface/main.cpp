//====================================================
//  Isosurface
//  Created by Viet Trinh on 3/5/13.
//  Copyright (c) 2013 Viet Trinh. All rights reserved.
//====================================================

#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <GLUT/glut.h>
#include <GLUI/glui.h>
#else
#include <GL/glut.h>
#include <GL/glui.h>
#endif

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <climits>
#include <unistd.h>
#include "PointAndVector.h"
#include "MarchingCubes.h"

#define PI 3.14159265359
using namespace std;

/*----- GLOBAL VARIABLES -------------*/
//=== GLUI variables ===
int window;
GLUI* glui;
GLUI_RadioGroup* choose_axis;
GLUI_RadioGroup* choose_color;

//=== GLUT variables ===
int button, state = 1;
float gx, gy;
float x_win = 600.0;
float z_win = 600.0;
float cube_size=150;
int   steps =255;
float vratio = cube_size/x_win;
float delta = 255.0/7.0;
float obj_material_amb[4] = {0.86,0.07,0.24,1.0};
float obj_material_diff[4] = {0.86,0.07,0.24,1.0};
float obj_material_spec[4] = {0.1,0.1,0.1,1.0};
float VIEWER[3]={100.0,400.0,100.0};
float LOOKAT[3]={static_cast<float>(x_win/2.0),0.0,static_cast<float>(z_win/2.0)};
Vector view_dir;           // direction vector from viewer to look-at-point

//=== Visualization variables ===
bool isIso =false;
float maxData = LONG_MIN;
float minData = LONG_MAX;
float A,B,C,D;
float isoValue=125;
int switchColor=0;
int numMCTriangles=0;
int moveUpDown_delta = 10;
int axis = 0;       // 0,1,2: x,y,z aligned-axises, 3: abitrary axis
int arbitrary_axis = 0;
int slices[6];      // slices[which-axis] contain information for current slice
// which-axis: x=0,y=1,z=2,a=3,4,5
// current slice's value is in [0 -> 255]
unsigned char Data[256][256][256];
MCPoint MCData[256*256*256];
Triangle* MCTriangles= NULL;

//=== Intermediate variables ===
Vector* color11 = new Vector();
Vector* color21 = new Vector();
Vector* color12 = new Vector();
Vector* color22 = new Vector();
Point* p1 = new Point();
Point* p2 = new Point();

Vector* C000 = new Vector();
Vector* C100 = new Vector();
Vector* C001 = new Vector();
Vector* C101 = new Vector();
Vector* C010 = new Vector();
Vector* C110 = new Vector();
Vector* C011 = new Vector();
Vector* C111 = new Vector();

Point* V0 = new Point();
Point* V1 = new Point();
Point* V2 = new Point();
Point* V3 = new Point();

Vector* colorV0 = new Vector();
Vector* colorV1 = new Vector();
Vector* colorV2 = new Vector();
Vector* colorV3 = new Vector();

/*----- FUNCTIONS DECLARATION --------*/
void init();
void screenSetUp();
void displayScreen();
void mouseClicks(int but,int sta,int x,int y);
void mouseMoves(int x, int y);
void keyPresses(unsigned char c, int x, int y);
void glui_cb(int control);
void glutIdle();

Vector* calculateNormal(Point* a, Point* b, Point* c);
void RotateLeftRight(float angle);
void MoveUpDown(float step);

void getData();
void DrawCubeFrame();
void DrawPlanes();
void DrawCell(int b0,int a0,int b1, int a1);
float getX(float x);
float getY(float A,float B,float C,float D,float x,float z);
float getZ(float z);
void ColorMapping(float density, Vector* ret);
void ColorInterpolation(int x0,int y0, int z0, float dx, float dy, float dz, Vector* ret);

void DrawIsosurface();
void LoadIsosurface(float isoValue);

//===== main function ==================================
int main(int argc, char**argv){
	
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	
	// Create a window
	window = glutCreateWindow("Visualization");
	glutPositionWindow(0, 0);
	glutReshapeWindow(x_win, z_win);
	
	// Program start here...
    /*----- GLUT -----*/
    getData();
    screenSetUp();
    glutDisplayFunc(displayScreen);
	GLUI_Master.set_glutMouseFunc(mouseClicks);
	glutMotionFunc(mouseMoves);
    GLUI_Master.set_glutKeyboardFunc(keyPresses);
    //GLUI_Master.set_glutIdleFunc(glutIdle);
    
    /*----- GLUI -----*/
    glui = GLUI_Master.create_glui_subwindow(window,GLUI_SUBWINDOW_BOTTOM);
    
    GLUI_Panel* axis_panel = glui->add_panel("Axis Panel",GLUI_PANEL_NONE);
    // sub panel 1 of axis_panel
    axis_panel->set_alignment(GLUI_ALIGN_CENTER);
    glui->add_statictext_to_panel(axis_panel,"CHOOSE AXIS");
    choose_axis = glui->add_radiogroup_to_panel(axis_panel,&axis);
    glui->add_radiobutton_to_group(choose_axis, "X-axis (x)");
    glui->add_radiobutton_to_group(choose_axis, "Y-axis (y)");
    glui->add_radiobutton_to_group(choose_axis, "Z-axis (z)");
    glui->add_radiobutton_to_group(choose_axis, "A-axis ");
    glui->add_radiobutton_to_group(choose_axis, "B-axis ");
    glui->add_radiobutton_to_group(choose_axis, "C-axis ");
    
    // sub panel 2 of axis_panel
    glui->add_column_to_panel(axis_panel,true);
    glui->add_statictext_to_panel(axis_panel,"CHOOSE COLOR");
    choose_color = glui->add_radiogroup_to_panel(axis_panel,&switchColor);
    glui->add_radiobutton_to_group(choose_color, "Blue Base");
    glui->add_radiobutton_to_group(choose_color, "Red Base");
    glui->add_statictext_to_panel(axis_panel," ");
    
    glui->add_column_to_panel(axis_panel,true);
    glui->add_statictext_to_panel(axis_panel,"MOVEMENT");
    glui->add_statictext_to_panel(axis_panel,"");
    glui->add_button_to_panel(axis_panel, "Up (>)"  ,0,glui_cb);
    glui->add_button_to_panel(axis_panel, "Down (<)",1,glui_cb);
    glui->add_button_to_panel(axis_panel, "Non/Iso Surface",2,glui_cb);
    
    glui->add_column_to_panel(axis_panel,true);
    glui->add_statictext_to_panel(axis_panel,"");
    glui->add_button_to_panel(axis_panel,"OK"  ,3,glui_cb);
    glui->add_button_to_panel(axis_panel,"Quit",4, (GLUI_Update_CB)exit)->set_alignment(GLUI_ALIGN_CENTER);
    
	glutMainLoop();
	return 0;
}

//===== screenSetUp ====================================
void screenSetUp(){
    
    /*------ SET UP 3D SCREEN -------*/
    float light_position[4] = { 0.0, 50.0, 300.0 , 0.0 };   // x, y, z, w
    // set up object color
    float obj_light_ambient[4] = { 0.2, 0.2, 0.2, 1.0 };     // r, g, b, a
    float obj_light_diffuse[4] = { 0.8, 0.3, 0.1, 1.0 };     // r, g, b, a
    float obj_light_specular[4] = { 0.8, 0.3, 0.1, 1.0 };    // r, g, b, a
    
    glClearColor(0.0,0.0,0.0,1.0);
    //glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0,x_win/z_win,5.0,5000.0);
    glViewport(0,0,x_win,z_win);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glEnable(GL_LIGHTING);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE);
    glEnable(GL_POINT_SMOOTH);
    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    
    glLightfv(GL_LIGHT0, GL_AMBIENT, obj_light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, obj_light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, obj_light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glEnable(GL_LIGHT0);
	
	// viewer is looking towards the center of the terrain
	gluLookAt(VIEWER[0],VIEWER[1],VIEWER[2],LOOKAT[0],LOOKAT[1],LOOKAT[2],0,1,0);
    
}

//===== displayScreen ===================================
void displayScreen(){
    
    // code here for drawing object...
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glPushMatrix();
    glTranslatef(x_win/2 - cube_size/2, 0, z_win/2 - cube_size/2);
    glScaled(1.2, 1.2, 1.2);
    // Rotate the cube for better angle view
    glPushMatrix();
    glTranslatef(cube_size/2, cube_size/2, cube_size/2);
    glRotatef(90, 1, 0, 0);
    glTranslatef((-1)*cube_size/2, (-1)*cube_size/2, (-1)*cube_size/2);
    DrawCubeFrame();
    if(!isIso) {
        DrawPlanes();
    }
    else {
        LoadIsosurface(isoValue);
        DrawIsosurface();
    }
    glPopMatrix();
    glPopMatrix();
    
    glutSwapBuffers();
}

//===== mouseClicks ====================================
void mouseClicks(int but,int sta,int x,int y){
    button = but;
    state = sta;
    
    
    if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN){
		
        gx = float(x)/x_win;
        gy = float(z_win-y)/z_win;
        
        // code here...
    }
    
    glutPostRedisplay();
    
}

//===== mouseMoves ====================================
void mouseMoves(int x, int y){
    
    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN){
		gx = float(x)/x_win;
        gy = float(z_win-y)/z_win;
        
        // code here..
        
    }
    
    glutPostRedisplay();
    
}

//===== keyPresses ====================================
void keyPresses(unsigned char c, int x, int y){
    
    glutPostRedisplay();
}

//=================================================================
void glui_cb(int control){
    
    /*--- Move up a slicing plane ---*/
    if(control ==0){
        if (!isIso) {
            int val = slices[axis];
            val = val+10;
            // make sure movement in range for x,y,z axis
            if(axis<3){
                if (val>254) {val = 254;}
            }
            // make sure movement in rangefor arbitraty axis
            else{
                int upper_limit = (int)(A*cube_size + C*cube_size);
                if(val >= upper_limit){val = upper_limit;}
                D = val;
            }
            slices[axis]=val;
        }
        else {
            isoValue = isoValue + 20;
        }
        
    }
    /*--- Move down a slicing plane ---*/
    else if (control==1) {
        if (!isIso) {
            int val = slices[axis];
            val = val-10;
            if (axis<3) {
                if (val<0) {val = 0;}
            }
            else{
                int low_limit =(int)(-1*B*cube_size);
                if(val <= low_limit){val = low_limit;}
                D = val;
            }
            slices[axis]=val;
        }
        else{
            isoValue = isoValue - 20;
        }
    }
    /*--- Non/Iso surface toggle ---*/
    else if (control == 2){
        if (isIso) {isIso = false;}
        else       {isIso = true;}
    }
    /*--- Changing arbitrary plane ---*/
    else if (control==3){
        
        if      (axis == 3){A=1;B=2;C=4;}
        else if (axis == 4){A=4;B=8;C=5;}
        else if (axis == 5){A=8;B=7;C=4;}
        
        if (axis>2) {
            D = 0.5*cube_size*(A-B+C);
            slices[axis] = (int)D;
        }
    }
    
    if (glutGetWindow()!= window) {
        glutSetWindow(window);
    }
    glutPostRedisplay();
}

//=================================================================
void glutIdle(){
    if (glutGetWindow()!= window) {
        glutSetWindow(window);
    }
    glutPostRedisplay();
    //sleep(100);
}

//=================================================================
Vector* calculateNormal(Point* a, Point* b, Point* c){
    /* Right hand rule: Thumb is the direction of normal vector,
     curve of fingers is the orientation of vertices
     n = ab x bc
     */
    
    Vector* ret = new Vector();
    Vector v1,v2,v;
    float d;
    
    // Vector ab
    v1.x = b->getXCoord() - a->getXCoord();
    v1.y = b->getYCoord() - a->getYCoord();
    v1.z = b->getZCoord() - a->getZCoord();
    
    // Vector bc
    v2.x = c->getXCoord() - b->getXCoord();
    v2.y = c->getYCoord() - b->getYCoord();
    v2.z = c->getZCoord() - b->getZCoord();
    
    // Vector ab x bc
    v.x = v1.y*v2.z - v2.y*v1.z;
    v.y = v1.z*v2.x - v2.z*v1.x;
    v.z = v1.x*v2.y - v2.x*v1.y;
    
    d = sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
    
    // Normalize vector ab x bc
    ret->x = v.x/d;
    ret->y = v.y/d;
    ret->z = v.z/d;
    
    return ret;
}

//=================================================================
void MoveUpDown(float step){
    
    float distance;
    
    // normalize direction vector
    view_dir.x = LOOKAT[0] - VIEWER[0];
    view_dir.y = LOOKAT[1] - VIEWER[1];
    view_dir.z = LOOKAT[2] - VIEWER[2];
    
    distance = sqrt(view_dir.x*view_dir.x + view_dir.y*view_dir.y + view_dir.z*view_dir.z);
    
    view_dir.x = view_dir.x/distance;
    view_dir.y = view_dir.y/distance;
    view_dir.z = view_dir.z/distance;
    
    // translate viewer and look-at-point positions
    VIEWER[0] += step*view_dir.x;
    VIEWER[2] += step*view_dir.z;
    LOOKAT[0] += step*view_dir.x;
    LOOKAT[2] += step*view_dir.z;
    
}

//=================================================================
void RotateLeftRight(float angle){
    float x1,y1,z1,x2,y2,z2;
    
    // translate to origin
    x1 = LOOKAT[0] - VIEWER[0];
    y1 = LOOKAT[1] - VIEWER[1];
    z1 = LOOKAT[2] - VIEWER[2];
    
    // rotate around a pivot
    x2 = x1*cos(angle) + z1*sin(angle);
    y2 = y1;
    z2 = -x1*sin(angle) + z1*cos(angle);
    
    // translate back
    LOOKAT[0] = x2 + VIEWER[0];
    LOOKAT[1] = y2 + VIEWER[1];
    LOOKAT[2] = z2 + VIEWER[2];
}

//=================================================================
void getData(){
    FILE * fptr;
    fptr = fopen("/Users/viettrinh/Desktop/CS116B/HWs/3/foot.raw", "rb");
    
    if (!fptr) {
        cout << "Could not load the file !!!"<<endl;
    }
    
    /*----- Storing file content to data -----*/
    cout<< "Loading foot.raw into program...."<<endl;
    for (int i=0; i<256; i++) {
        for (int j=0; j<256; j++) {
            for (int k=0; k<256; k++) {
                int n = fgetc(fptr);
                Data[i][j][k] = (unsigned char)n;
                if (minData>n) {
                    minData = n;
                }
                if (maxData<n) {
                    maxData = n;
                }
            }
        }
    }
    isIso = false;
    cout<< "Loading foot.row: DONE"<<endl<<"Cube Display ...."<<endl;
    /*
     cout<< "Testing DATA after loading..."<<endl;
     cout<< "MAX= "<<maxData<<endl;
     cout<< "MIN= "<<minData<<endl;
     */
    
    /*----- Initialize all other data and variables-----*/
    // initialize every aligned-axis start at slice 127
    for (int i=0; i<3; i++) {
        slices[i] = 127;
    }
    
    // Initialize coefficient for an arbitray plane
    // Ax - By + Cz - D = 0
    // where A,B,C >0
    
    A = 4;B=8;C=5;
    D = 0.5*cube_size*(A-B+C);
    slices[4] = (int)D;
    
    A = 8;B=7;C=4;
    D = 0.5*cube_size*(A-B+C);
    slices[5] = (int)D;
    
    A = 1;B=2;C=4;
    D = 0.5*cube_size*(A-B+C);
    slices[3] = (int)D;
}

//=================================================================
void DrawCubeFrame(){
    
    /*--- Set the color red for the cube frame ---*/
    obj_material_amb[0] = 0.86;
    obj_material_amb[1] = 0.07;
    obj_material_amb[2] = 0.24;
    obj_material_amb[3] = 1.0;
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, obj_material_amb);
    
    /*--- Draw the cube frame of size cube_size ---*/
    glBegin(GL_LINE_STRIP);
    glVertex3f(0, 0, 0);
    glVertex3f(cube_size, 0, 0);
    glVertex3f(cube_size, cube_size, 0);
    glVertex3f(0, cube_size, 0);
    glEnd();
    
    glBegin(GL_LINE_STRIP);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, cube_size);
    glVertex3f(0, cube_size, cube_size);
    glVertex3f(0, cube_size, 0);
    glEnd();
    
    glBegin(GL_LINE_STRIP);
    glVertex3f(0, 0, cube_size);
    glVertex3f(cube_size, 0, cube_size);
    glVertex3f(cube_size, cube_size, cube_size);
    glVertex3f(0, cube_size, cube_size);
    glEnd();
    
    glBegin(GL_LINES);
    glVertex3f(cube_size, cube_size, cube_size);
    glVertex3f(cube_size, cube_size, 0);
    glVertex3f(cube_size, 0, cube_size);
    glVertex3f(cube_size, 0, 0);
    glVertex3f(0, cube_size, 0);
    glVertex3f(0, 0, 0);
    glEnd();
    
}

//=================================================================
void DrawPlanes(){
    
    /*===== Draw outline for a display plane =====*/
    /*--- X-axis ---*/
    if(axis==0){
        glBegin(GL_LINE_STRIP);
        glVertex3f(slices[axis]*(cube_size/255.0), 0, 0);
        glVertex3f(slices[axis]*(cube_size/255.0), cube_size, 0);
        glVertex3f(slices[axis]*(cube_size/255.0), cube_size, cube_size);
        glVertex3f(slices[axis]*(cube_size/255.0), 0, cube_size);
        glVertex3f(slices[axis]*(cube_size/255.0), 0, 0);
        glEnd();
    }
    /*--- Y-axis ---*/
    else if(axis==1){
        glBegin(GL_LINE_STRIP);
        glVertex3f(0, slices[axis]*(cube_size/255.0), 0);
        glVertex3f(cube_size, slices[axis]*(cube_size/255.0), 0);
        glVertex3f(cube_size, slices[axis]*(cube_size/255.0), cube_size);
        glVertex3f(0, slices[axis]*(cube_size/255.0), cube_size);
        glVertex3f(0, slices[axis]*(cube_size/255.0), 0);
        glEnd();
    }
    /*--- Z-axis ---*/
    else if(axis==2){
        glBegin(GL_LINE_STRIP);
        glVertex3f(0, 0, slices[axis]*(cube_size/255.0));
        glVertex3f(0, cube_size, slices[axis]*(cube_size/255.0));
        glVertex3f(cube_size, cube_size, slices[axis]*(cube_size/255.0));
        glVertex3f(cube_size, 0, slices[axis]*(cube_size/255.0));
        glVertex3f(0, 0, slices[axis]*(cube_size/255.0));
        glEnd();
    }
    /*--- Arbitrary-axis ---*/
    else if (axis==3){
        Vector P0,P1,P2,P3;
        
        P0.x = 0;
        P0.y = 0;
        P0.z = D/C;
        if(P0.z > cube_size){P0.z = cube_size;}
        if(P0.z <0){P0.z = 0;}
        
        P1.x = 0;
        P1.y = cube_size;
        P1.z = (B*cube_size+D)/C;
        if(P1.z > cube_size){P1.z = cube_size;}
        if(P1.z <0){P1.z = 0;}
        
        P2.x = cube_size;
        P2.y = cube_size;
        P2.z = (B*cube_size-A*cube_size+D)/C;
        if(P2.z > cube_size){P2.z = cube_size;}
        if(P2.z <0){P2.z = 0;}
        
        P3.x = cube_size;
        P3.y = 0;
        P3.z = (D-A*cube_size)/C;
        if(P3.z > cube_size){P3.z = cube_size;}
        if(P3.z <0){P3.z = 0;}
        
        glBegin(GL_LINE_STRIP);
        glVertex3f(P0.x, P0.y, P0.z);
        glVertex3f(P1.x, P1.y, P1.z);
        glVertex3f(P2.x, P2.y, P2.z);
        glVertex3f(P3.x, P3.y, P3.z);
        glVertex3f(P0.x, P0.y, P0.z);
        glEnd();
    }
    
    /*===== Draw display 2D plane =====*/
    // Enable GL_SMOOTH for color linear interpolation
    glShadeModel(GL_SMOOTH);
    for (int y=0; y<steps; y++) {
        for (int x=0; x<steps; x++) {
            DrawCell(y, x, y+1, x+1);
        }
    }
}

//=================================================================
void DrawCell(int b0,int a0,int b1, int a1){
    
    int x1 =0,y1=0,z1=0,x2=0,y2=0,z2=0;
    
    /*-------- ALIGNED AXISES: X-AXIS, Y-AXIS, Z-AXIS --------*/
    if (axis<3) {
        /*===== Assign coordinate and color mapping for a display plane =====*/
        /*--- X-axis ---*/
        if (axis==0) {
            x1=slices[axis];
            x2=slices[axis];
            y1=b0;
            y2=b1;
            z1=a0;
            z2=a1;
            ColorMapping(Data[x1][y1][z1], color11);
            ColorMapping(Data[x1][y1][z2], color12);
            ColorMapping(Data[x1][y2][z1], color21);
            ColorMapping(Data[x1][y2][z2], color22);
        }
        /*--- Y-axis ---*/
        else if (axis==1) {
            x1=a0;
            x2=a1;
            y1=slices[axis];
            y2=slices[axis];
            z1=b0;
            z2=b1;
            ColorMapping(Data[x1][y1][z1], color11);
            ColorMapping(Data[x2][y1][z1], color12);
            ColorMapping(Data[x1][y1][z2], color21);
            ColorMapping(Data[x2][y1][z2], color22);
        }
        /*--- Z-axis ---*/
        else if (axis==2) {
            x1=a0;
            x2=a1;
            y1=b0;
            y2=b1;
            z1=slices[axis];
            z2=slices[axis];
            ColorMapping(Data[x1][y1][z1], color11);
            ColorMapping(Data[x1][y2][z1], color12);
            ColorMapping(Data[x2][y1][z1], color21);
            ColorMapping(Data[x2][y2][z1], color22);
        }
        
        p1->setXYZCoord(x1*(cube_size/255.0),y1*(cube_size/255.0),z1*(cube_size/255.0));
        p2->setXYZCoord(x2*(cube_size/255.0),y2*(cube_size/255.0),z2*(cube_size/255.0));
        
        /*===== Display plane =====*/
        /*--- 1st triangle ---*/
        glBegin(GL_POLYGON);
        
        /*--- vertex 1 ---*/
        obj_material_amb[0]=color11->x;
        obj_material_amb[1]=color11->y;
        obj_material_amb[2]=color11->z;
        obj_material_amb[3]=1.0;
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, obj_material_amb);
        if      (axis==0){glVertex3f(p1->getXCoord(), p1->getYCoord(), p1->getZCoord());}
        else if (axis==1){glVertex3f(p1->getXCoord(), p1->getYCoord(), p1->getZCoord());}
        else if (axis==2){glVertex3f(p1->getXCoord(), p1->getYCoord(), p1->getZCoord());}
        
        /*--- vertex 2 ---*/
        obj_material_amb[0]=color12->x;
        obj_material_amb[1]=color12->y;
        obj_material_amb[2]=color12->z;
        obj_material_amb[3]=1.0;
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, obj_material_amb);
        if      (axis==0){glVertex3f(p1->getXCoord(), p1->getYCoord(), p2->getZCoord());}
        else if (axis==1){glVertex3f(p2->getXCoord(), p1->getYCoord(), p1->getZCoord());}
        else if (axis==2){glVertex3f(p1->getXCoord(), p2->getYCoord(), p1->getZCoord());}
        
        /*--- vertex 3 ---*/
        obj_material_amb[0]=color21->x;
        obj_material_amb[1]=color21->y;
        obj_material_amb[2]=color21->z;
        obj_material_amb[3]=1.0;
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, obj_material_amb);
        if      (axis==0) {glVertex3f(p1->getXCoord(), p2->getYCoord(), p1->getZCoord());}
        else if (axis==1){glVertex3f(p1->getXCoord(), p1->getYCoord(), p2->getZCoord());}
        else if (axis==2){glVertex3f(p2->getXCoord(), p1->getYCoord(), p1->getZCoord());}
        
        glEnd();
        
        /*--- 2nd triangle ---*/
        glBegin(GL_POLYGON);
        
        /*--- vertex 1 ---*/
        obj_material_amb[0]=color22->x;
        obj_material_amb[1]=color22->y;
        obj_material_amb[2]=color22->z;
        obj_material_amb[3]=1.0;
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, obj_material_amb);
        if      (axis==0){glVertex3f(p1->getXCoord(), p2->getYCoord(), p2->getZCoord()); }
        else if (axis==1){ glVertex3f(p2->getXCoord(), p1->getYCoord(), p2->getZCoord());}
        else if (axis==2){glVertex3f(p2->getXCoord(), p2->getYCoord(), p1->getZCoord());}
        
        /*--- vertex 2 ---*/
        obj_material_amb[0]=color12->x;
        obj_material_amb[1]=color12->y;
        obj_material_amb[2]=color12->z;
        obj_material_amb[3]=1.0;
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, obj_material_amb);
        if      (axis==0){ glVertex3f(p1->getXCoord(), p1->getYCoord(), p2->getZCoord());}
        else if (axis==1){glVertex3f(p2->getXCoord(), p1->getYCoord(), p1->getZCoord());}
        else if (axis==2){glVertex3f(p1->getXCoord(), p2->getYCoord(), p1->getZCoord());}
        
        /*--- vertex 3 ---*/
        obj_material_amb[0]=color21->x;
        obj_material_amb[1]=color21->y;
        obj_material_amb[2]=color21->z;
        obj_material_amb[3]=1.0;
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, obj_material_amb);
        if      (axis==0){glVertex3f(p1->getXCoord(), p2->getYCoord(), p1->getZCoord());}
        else if (axis==1){glVertex3f(p1->getXCoord(), p1->getYCoord(), p2->getZCoord());}
        else if (axis==2){glVertex3f(p2->getXCoord(), p1->getYCoord(), p1->getZCoord());}
        
        glEnd();
    } // end axis<3
    
    /*-------- NON-ALIGNED AXIS: ARBITRARY AXIS N(A,B,C) --------*/
    else{
        float pV_z = (float)D/C;
        float x_coef = cube_size/255.0;
        float z_coef = (B*cube_size/C)/255.0;
        
        /*===== 4 intersection points btw plane and cube =====*/
        V0->setXYZCoord(getX(a0*x_coef),getY(A, B, C, D, a0*x_coef, pV_z+b0*z_coef),getZ(pV_z+b0*z_coef));
        V1->setXYZCoord(getX(a0*x_coef),getY(A, B, C, D, a0*x_coef, pV_z+b1*z_coef),getZ(pV_z+b1*z_coef));
        V2->setXYZCoord(getX(a1*x_coef),getY(A, B, C, D, a1*x_coef, pV_z+b1*z_coef),getZ(pV_z+b1*z_coef));
        V3->setXYZCoord(getX(a1*x_coef),getY(A, B, C, D, a1*x_coef, pV_z+b0*z_coef),getZ(pV_z+b0*z_coef));
        
        // Get color of the 1st intersection point V0
        float V0_x = (V0->getXCoord())/x_coef;
        float V0_y = (V0->getYCoord())/x_coef;
        float V0_z = (V0->getZCoord())/x_coef;
        int int_V0_x = (int)V0_x;
        int int_V0_y = (int)V0_y;
        int int_V0_z = (int)V0_z;
        ColorInterpolation(int_V0_x,int_V0_y,int_V0_z,                                  // x0,y0,z0
                           V0_x - int_V0_x,V0_y - int_V0_y,V0_z - int_V0_z, colorV0);  // dx,dy,dz
        
        // Get color of the 2nd intersection point V1
        float V1_x = (V1->getXCoord())/x_coef;
        float V1_y = (V1->getYCoord())/x_coef;
        float V1_z = (V1->getZCoord())/x_coef;
        int int_V1_x = (int)V1_x;
        int int_V1_y = (int)V1_y;
        int int_V1_z = (int)V1_z;
        ColorInterpolation(int_V1_x,int_V1_y,int_V1_z,                                  // x0,y0,z0
                           V1_x - int_V1_x,V1_y - int_V1_y,V1_z - int_V1_z, colorV1);  // dx,dy,dz
        
        // Get color of the 3rd intersection point V2
        float V2_x = (V2->getXCoord())/x_coef;
        float V2_y = (V2->getYCoord())/x_coef;
        float V2_z = (V2->getZCoord())/x_coef;
        int int_V2_x = (int)V2_x;
        int int_V2_y = (int)V2_y;
        int int_V2_z = (int)V2_z;
        ColorInterpolation(int_V2_x,int_V2_y,int_V2_z,                                  // x0,y0,z0
                           V2_x - int_V2_x,V2_y - int_V2_y,V2_z - int_V2_z, colorV2);  // dx,dy,dz
        
        // Get color of the 4th intersection point V3
        float V3_x = (V3->getXCoord())/x_coef;
        float V3_y = (V3->getYCoord())/x_coef;
        float V3_z = (V3->getZCoord())/x_coef;
        int int_V3_x = (int)V3_x;
        int int_V3_y = (int)V3_y;
        int int_V3_z = (int)V3_z;
        ColorInterpolation(int_V3_x,int_V3_y,int_V3_z,                                  // x0,y0,z0
                           V3_x - int_V3_x,V3_y - int_V3_y,V3_z - int_V3_z, colorV3);  // dx,dy,dz
        
        /*===== Display plane =====*/
        /*--- 1st triangle ---*/
        glBegin(GL_POLYGON);
        obj_material_amb[0]=colorV0->x;
        obj_material_amb[1]=colorV0->y;
        obj_material_amb[2]=colorV0->z;
        obj_material_amb[3]=1.0;
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, obj_material_amb);
        glVertex3f(V0->getXCoord(), V0->getYCoord(), V0->getZCoord());
        
        obj_material_amb[0]=colorV1->x;
        obj_material_amb[1]=colorV1->y;
        obj_material_amb[2]=colorV1->z;
        obj_material_amb[3]=1.0;
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, obj_material_amb);
        glVertex3f(V1->getXCoord(), V1->getYCoord(), V1->getZCoord());
        
        obj_material_amb[0]=colorV3->x;
        obj_material_amb[1]=colorV3->y;
        obj_material_amb[2]=colorV3->z;
        obj_material_amb[3]=1.0;
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, obj_material_amb);
        glVertex3f(V3->getXCoord(), V3->getYCoord(), V3->getZCoord());
        glEnd();
        
        /*--- 2nd triangle ---*/
        glBegin(GL_POLYGON);
        obj_material_amb[0]=colorV2->x;
        obj_material_amb[1]=colorV2->y;
        obj_material_amb[2]=colorV2->z;
        obj_material_amb[3]=1.0;
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, obj_material_amb);
        glVertex3f(V2->getXCoord(), V2->getYCoord(), V2->getZCoord());
        
        obj_material_amb[0]=colorV1->x;
        obj_material_amb[1]=colorV1->y;
        obj_material_amb[2]=colorV1->z;
        obj_material_amb[3]=1.0;
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, obj_material_amb);
        glVertex3f(V1->getXCoord(), V1->getYCoord(), V1->getZCoord());
        
        obj_material_amb[0]=colorV3->x;
        obj_material_amb[1]=colorV3->y;
        obj_material_amb[2]=colorV3->z;
        obj_material_amb[3]=1.0;
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, obj_material_amb);
        glVertex3f(V3->getXCoord(), V3->getYCoord(), V3->getZCoord());
        glEnd();
    }// end axis==3
}

//=================================================================
void ColorMapping(float density, Vector* ret){
    
    /*--- main color is BLUE ---*/
    if(switchColor == 0){
        if (density>=0 && density <= delta) {
            ret->x = 0;
            ret->y = 0;
            ret->z = density/delta;
        }
        else if (density>delta && density <= (2*delta)) {
            ret->x = (density-delta)/delta;
            ret->y = 0;
            ret->z = 1;
        }
        else if (density>(2*delta) && density <= (3*delta)) {
            ret->x = 1;
            ret->y = 0;
            ret->z = 1-(density-2*delta)/delta;
        }
        else if (density>(3*delta) && density <= (4*delta)) {
            ret->x = 1;
            ret->y = (density-3*delta)/delta;
            ret->z = 0;
        }
        else if (density>(4*delta) && density <= (5*delta)) {
            ret->x = 1-(density-4*delta)/delta;
            ret->y = 1;
            ret->z = 0;
        }
        else if (density>(5*delta) && density <= (6*delta)) {
            ret->x = 0;
            ret->y = 1;
            ret->z = (density-5*delta)/delta;
        }
        else if (density>(6*delta) && density <= (7*delta)) {
            ret->x = (density-6*delta)/delta;
            ret->y = 1;
            ret->z = 1;
        }
        else {;}
    }
    
    /*--- main color is RED ---*/
    else{
        if (density>=0 && density <= delta) {
            ret->x = density/delta;
            ret->y = 0;
            ret->z = 0;
        }
        else if (density>delta && density <= (2*delta)) {
            ret->x = 1;
            ret->y = (density-delta)/delta;
            ret->z = 0;
        }
        else if (density>(2*delta) && density <= (3*delta)) {
            ret->x = 1-(density-2*delta)/delta;
            ret->y = 1;
            ret->z = 0;
        }
        else if (density>(3*delta) && density <= (4*delta)) {
            ret->x = 0;
            ret->y = 1;
            ret->z = (density-3*delta)/delta;
        }
        else if (density>(4*delta) && density <= (5*delta)) {
            ret->x = 0;
            ret->y = 1-(density-4*delta)/delta;
            ret->z = 1;
        }
        else if (density>(5*delta) && density <= (6*delta)) {
            ret->x = (density-5*delta)/delta;
            ret->y = 0;
            ret->z = 1;
        }
        else if (density>(6*delta) && density <= (7*delta)) {
            ret->x = 1;
            ret->y = (density-6*delta)/delta;
            ret->z = 1;
        }
        else {;}
    }
    
}

//=================================================================
float getX(float x){
    float retX = x;
    if(retX>cube_size){retX=cube_size;}
    if(retX<0){retX=0;}
    return retX;
}

//=================================================================
float getY(float A,float B,float C,float D,float x,float z){
    float y = (A*x+C*z-D)/B;
    if(y>cube_size){y=cube_size;}
    if(y<0){y=0;}
    return y;
}

//=================================================================
float getZ(float z){
    float retZ = z;
    if(retZ>cube_size){retZ=cube_size;}
    if(retZ<0){retZ=0;}
    return retZ;
}

//=================================================================
void ColorInterpolation(int x0,int y0, int z0, float dx, float dy, float dz, Vector* ret){
    int x1, y1, z1;
    
    x1 = x0+1;
    if (x1>steps) {x1 = steps;}
    
    y1 = y0+1;
    if (y1>steps) {y1 = steps;}
    
    z1 = z0+1;
    if (z1>steps) {z1 = steps;}
    
    /*----- get colors of 8 corners -----*/
    ColorMapping(Data[x0][y0][z0], C000);
    ColorMapping(Data[x1][y0][z0], C100);
    ColorMapping(Data[x0][y0][z1], C001);
    ColorMapping(Data[x1][y0][z1], C101);
    ColorMapping(Data[x0][y1][z0], C010);
    ColorMapping(Data[x1][y1][z0], C110);
    ColorMapping(Data[x0][y1][z1], C011);
    ColorMapping(Data[x1][y1][z1], C111);
    
    /*----- Tri-linear interpolation for getting color an inside point -----*/
    float   Cx00_R,Cx01_R,Cx10_R,Cx11_R,Cx0z_R,Cx1z_R,Cxyz_R,
    Cx00_G,Cx01_G,Cx10_G,Cx11_G,Cx0z_G,Cx1z_G,Cxyz_G,
    Cx00_B,Cx01_B,Cx10_B,Cx11_B,Cx0z_B,Cx1z_B,Cxyz_B;
    
    Cx00_R = (C000->x)*(1-dx) + (C100->x)*dx;
    Cx01_R = (C001->x)*(1-dx) + (C101->x)*dx;
    Cx10_R = (C010->x)*(1-dx) + (C110->x)*dx;
    Cx11_R = (C011->x)*(1-dx) + (C111->x)*dx;
    Cx0z_R = Cx00_R*(1-dz) + Cx01_R*dz;
    Cx1z_R = Cx10_R*(1-dz) + Cx11_R*dz;
    Cxyz_R = Cx0z_R*(1-dy) + Cx1z_R*dy;
    
    Cx00_G = (C000->y)*(1-dx) + (C100->y)*dx;
    Cx01_G = (C001->y)*(1-dx) + (C101->y)*dx;
    Cx10_G = (C010->y)*(1-dx) + (C110->y)*dx;
    Cx11_G = (C011->y)*(1-dx) + (C111->y)*dx;
    Cx0z_G = Cx00_G*(1-dz) + Cx01_G*dz;
    Cx1z_G = Cx10_G*(1-dz) + Cx11_G*dz;
    Cxyz_G = Cx0z_G*(1-dy) + Cx1z_G*dy;
    
    Cx00_B = (C000->z)*(1-dx) + (C100->z)*dx;
    Cx01_B = (C001->z)*(1-dx) + (C101->z)*dx;
    Cx10_B = (C010->z)*(1-dx) + (C110->z)*dx;
    Cx11_B = (C011->z)*(1-dx) + (C111->z)*dx;
    Cx0z_B = Cx00_B*(1-dz) + Cx01_B*dz;
    Cx1z_B = Cx10_B*(1-dz) + Cx11_B*dz;
    Cxyz_B = Cx0z_B*(1-dy) + Cx1z_B*dy;
    
    ret->x = Cxyz_R;
    ret->y = Cxyz_G;
    ret->z = Cxyz_B;
    
}

//=================================================================
void LoadIsosurface(float isoValue){
    
    // Deallocate memory for old images of old isovalue
    if (MCTriangles!=NULL) {
        delete [] MCTriangles;
    }
    
    cout << "ISOSURFACE: Loading ..."<<endl;
    for (int i=0; i<256; i++) {
        for (int j=0; j<256; j++) {
            for (int k=0 ; k<256; k++) {
                int index = i*256*256+j*256+k;
                MCData[index].setX(((float)i)*(cube_size/255.0));
                MCData[index].setY(((float)j)*(cube_size/255.0));
                MCData[index].setZ(((float)k)*(cube_size/255.0));
                MCData[index].setValue(Data[i][j][k]);
            }
        }
    }
    
    // Marching Cubes algorithm
    MCTriangles = MarchingCubes(steps, steps, steps, isoValue, MCData, numMCTriangles);
    cout << "ISOSURFACE: DISPLAY"<<endl;
}

//=================================================================
void DrawIsosurface(){
    
    if (MCTriangles == NULL) {
        cout << "Marching Cubes Algorithm FAILED;"<<endl;
    }
    else{
        Point p0,p1,p2;
        Vector * norm = new Vector();
        for (int i=0; i<numMCTriangles; i++) {
            p0.setXYZCoord(MCTriangles[i].vertices[0].getXCoord(), MCTriangles[i].vertices[0].getYCoord(), MCTriangles[i].vertices[0].getZCoord());
            p1.setXYZCoord(MCTriangles[i].vertices[1].getXCoord(), MCTriangles[i].vertices[1].getYCoord(), MCTriangles[i].vertices[1].getZCoord());
            p2.setXYZCoord(MCTriangles[i].vertices[2].getXCoord(), MCTriangles[i].vertices[2].getYCoord(), MCTriangles[i].vertices[2].getZCoord());
            
            norm = calculateNormal(&p0, &p1, &p2);
            
            glBegin(GL_POLYGON);
            
            obj_material_amb[0]=0.6;
            obj_material_amb[1]=0.6;
            obj_material_amb[2]=0.6;
            obj_material_amb[3]=1.0;
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, obj_material_amb);
            
            glNormal3f(norm->x, norm->y, norm->z);
            glVertex3f(p0.getXCoord(), p0.getYCoord(), p0.getZCoord());
            glVertex3f(p1.getXCoord(), p1.getYCoord(), p1.getZCoord());
            glVertex3f(p2.getXCoord(), p2.getYCoord(), p2.getZCoord());
            glEnd();
        }
        
        
    }
}
